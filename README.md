# PROYECTO DINAMITA: Cálculo de Tensiones (12/10 - 05/12 - 26/12)

##### En el presente proyecto se realizará un cálculo de las tensiones de una situación física. En primer lugar se solicitará al usuario que ingrese los ángulos de las cuerdas que tiran de una masa y el valor de esta, según la imagen referencial del problema físico. El programa tomará estos datos y, por medio de cálculos y fórmulas, arrojará como resultado el valor de las tensiones correspondiente a cada cuerda.
##### Con ayuda de la interfaz gráfica Qt Creator se graficarán proporcionalmente los vectores que representan a las diversas tensiones previamente calculadas.

##### Realizado por:
- Catalina González: 202130010-4
- Nicolas Verdugo: 202012033-1

#### **Diagrama de componentes:**
##### Está incluído en una de las imagenes adjuntas del repositorio como "Diagrama.png"

#### **Requisitos de Compilación o Plataforma de Instalación**
##### A partir de la segunda entrega de este proyecto se implementó QtCreator, que nos permitirá tener el programa completo del proyecto, por lo tanto, es necesario tenerlo instalado, preferentemente cualquiera de las versiones 5.0, ya que las más recientes generaron problemas de configuración.

### **1. Introducción**

##### Proyecto Dinamita es un programa realizado en lenguaje de programación C++, por medio de la IDE de QtCreator y su derivado repositorio ha sido reservado en la plataforma Gitlab.
##### Este proyecto recibe un problema físico-matemático basado en tensión de cuerdas (fuerzas-mecánica), basado en la imagen referencial adjunta, en el cual se le solicita al usuario ingresar magnitudes de ángulos en [°], que corresponden a la inclinación de las cuerdas, y el valor de una masa en unidad de medida equivalente a kilogramos [kg]. Estos datos se reemplazan en una ecuación matemática que nos permite obtener el resultado de las tensiones ejercidas en dicho ejercicio como magnitudes escalares y como vectores proporcionales en tamaño.

### **2. Descarga/Instalación**
#### 2.1 Requisitos
##### Tener instalado Qt Creator, en nuestro caso funcionaron de las versiones 5.0 en adelante.
#### 2.2 Instalación
##### A continuación le dejamos un link para descargar Qt Creator, en el cual es necesario además registrarse https://www.qt.io/download
##### Para la ejecución del programa en QtCreator sólo tiene que descargar el archivo .rar ubicado en el repositorio con el nombre de "TensionometroFinalConInterfaz.rar". Luego de descargarlo, en el interior encontrará los archivos en la carpeta "Interfaz" que debe ejecutar en el fichero correspondiente de la plataforma QtCreator. 

#### 2.3 Compilación
##### El proyecto está preparado para ser compilado usando Qt Creator insertando el código previamente nombrado en la IDE correspondiente.

### **Referencias**
##### Cálculo de tensiones: https://www.youtube.com/watch?v=6ixpojuaFOE
##### Código modular de las ayudantías: https://drive.google.com/file/d/1XLHWrZfGihaK8oO-NwoQlYZFs-PKETuJ/view
##### Cómo usar Qt Creator: https://drive.google.com/file/d/1228ZONuB4DpZfu1wfXk6i58lZdGZXJBx/view
##### Para dibujar los vectores: https://programmerclick.com/article/47141902583/

